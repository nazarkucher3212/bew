// requires...

// constants...
const extensions = {'log': true, 'txt': true, 'json': true, yaml: true, xml: true, js: true}
const fs = require("fs");
const e = require("express");
const path = './files/'

function createFile(req, res, next) { //ok
    let data = req.body
    let extension;
    if (data.filename) {
        extension = data.filename.split('.').pop()
    } else {
        next({"message": "undefined extension", status: 400})
    }
    if (extensions[extension]) {
        fs.writeFileSync(`${path}${data.filename}`, data.content)
    } else {
        next({"message": "undefined extension", status: 400})
        return
    }

    res.status(200).send({"message": "File created successfully"});
}

function getFiles(req, res, next) {
    let files = fs.readdirSync(path)
    res.status(200).send({
        "message": "Success", "files": files
    });
}

const getFile = (req, res, next) => { //ok
    let filename = req.params.filename

    if (!fs.existsSync(path + filename)) {
        next({"message": "no such file", status: 400})
        return
    }

    let extension = filename.split('.').pop()
    let content = fs.readFileSync(path + filename, {encoding: 'utf8', flag: 'r'})
    let {birthtime} = fs.statSync(path + filename)

    res.status(200).send({
        "message": "Success",
        "filename": filename,
        "content": content,
        "extension": extension,
        "uploadedDate": birthtime
    });
}

// Other functions - editFile, deleteFile
const editFile = (req, res, next) => {
    let data = req.body

    if (!fs.existsSync(path + data.filename)) {
        next({"message": "no such file", status: 400})
        return
    }

    fs.writeFileSync(`${path}${data.filename}`, data.content, {flag: 'a'})

    res.status(200).send({"message": "File created successfully"});
}

const deleteFile = (req, res, next) => {
    let data = req.body

    if (!fs.existsSync(path + data.filename)) {
        next({"message": "no such file", status: 400})
        return
    }

    fs.unlinkSync(`${path}${data.filename}`)

    res.status(200).send({"message": "File deleted successfully"});
}

module.exports = {
    createFile, getFiles, getFile, editFile, deleteFile
}
